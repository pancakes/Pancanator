exception Empty_List;;

(*********************
 * Fonction de debuggage - affichage de l'arbre
 * Cette fonction affiche un noeud à la auteur passée en paramètre
 * arbre est un 'a arbre espace est une chaine contenant des espace, tipe est
 * une fonction qui convertit le type d'arbre en string
 * *******************)
let rec string_of_arbre arbr espace tipe =
  match arbr with
  | Noeud(a,root,b) -> 
      (string_of_arbre b (espace ^ "    ") tipe) ^
      (espace ^ "  /\n") ^ (espace ^ (tipe root) ^ "\n") ^ (espace ^ "  \\\n") ^
      (string_of_arbre a (espace ^ "    ") tipe)
  | Nul -> ""
;;

(**********************
 * fonction de debuggage - affiche l'arbre
 * *******************)
let affiche arbr tipe =
  print_string ((string_of_arbre arbr "" tipe)^ "\n\n");;


(**********************
 * Calcul l'antropie pour un attribut
 * Précond  : liste_rep est une liste de couple contenant une valeur et un
 *              ternaire
 *            value est un entier représentant l'antropie sur -∞,+∞
 * Postcond : Ajoute 1 pour chaque animal à Vrai, retire 1 pour chaque animal à
 *              Faux.
 *            Pour pénaliser la présence d'animal dont la réponse est inconnu,
 *              on multiplie l'antropie par 2 si elle est différente de 0, sinon
 *              on ajoute 2.
 * *******************)
let rec antropi liste_rep value =
  match liste_rep  with
  | (_,True)::r -> antropi r (value + 1)
  | (_,Blank)::r when (value = 0) -> antropi r (value + 2)
  | (_,Blank)::r -> antropi r (value * 2)
  | (_,_)::r -> antropi r (value - 1)
  | [] -> value
;;

(*********************
 * Applique le calcul d'antropie sur tous les attributs
 * Précond  : liste_attr est une liste de data
 * Postcond : Renvoie une liste de couple avec la valeur absolue de l'antropie
 *              pour l'attribut et la data correspondant à l'attribut 
 * ******************)
let rec antropi_all liste_attr =
  match liste_attr with
  | Data(attr,liste_rep)::r -> (abs (antropi liste_rep 0),
                                Data(attr,liste_rep))::(antropi_all r)
  | [] -> []
;;

(*********************
 * Compare deux couple en fonction de la première valeur du couple
 * Précond  : a et b sont deux couple, leur première valeur est comparable avec >
 * Postcond : Renvoi 1 si a > b, -1 si a < b, 0 si a = b
 * ******************)
let compare_antrop a b =
  match a,b with
  | (antro1,_),(antro2,_) when (antro1 > antro2) -> 1
  | (antro1,_),(antro2,_) when (antro1 < antro2) -> (-1)
  | _ -> 0
;;

(*********************
 * Trie la liste d'attribut en fonction de l'antropie
 * Précond  : liste_attr est une liste de data
 * Postcond : Récupere seulement les data de la liste trié de couple
 * antropie,data
 * ******************)
let chose_attr liste_attr =
  List.map (fun (antro,attr) -> attr) (List.fast_sort compare_antrop (antropi_all liste_attr))
;;

(********************
 * 
 * *****************)
let rec maj_replist liste_paschoisi liste_choisi sens =
  match liste_paschoisi with
  | [] -> []
  | animal::reste ->  match liste_choisi with
                      | (a,cur_sens)::r when ((cur_sens = sens) || (cur_sens = Blank)) -> animal::(maj_replist reste r sens)
                      | (_,_)::r -> maj_replist reste r sens
                      | [] -> []
;;

let allsame liste_rep =
  ((List.for_all (fun (a,x) -> (x = False) || (x = Blank)) liste_rep) ||
  (List.for_all (fun (a,x) -> (x = True) || (x = Blank)) liste_rep))
;;

let rec maj_datalist liste_data liste_choisi sens =
  match liste_data with
  | Data(attr,liste_rep)::r -> let new_liste = maj_replist liste_rep liste_choisi sens in 
      if  ( not (allsame new_liste)) then
        (Data(attr,new_liste))::(maj_datalist r liste_choisi sens)
      else
        maj_datalist r liste_choisi sens
  | [] -> []
;;

let rec select liste sens =
  match liste with
  | (animal,boo)::r when (boo = sens) -> animal::(select r sens)
  | (animal,Blank)::r -> animal::(select r sens) 
  | a::r -> select r sens
  | [] -> []
;;

let affiche_anim liste =
  List.fold_left 
  (fun a (ainm,p) -> 
    print_string ( "[" ^ ainm ^ "," ^ (string_of_ternaire p) ^"];"); "toto") 
  "toto" liste ;;

let rec build_pancanator liste_data animaux_rest sens=
  match liste_data with
  | a::r -> 
      let data_list_trie = chose_attr liste_data in
      let Data(meilleur_attr,liste_choisi) = List.hd data_list_trie in
      creerArbre 
        (build_pancanator (maj_datalist (List.tl data_list_trie) liste_choisi True) liste_choisi True) 
        ([meilleur_attr]) 
        (build_pancanator (maj_datalist (List.tl data_list_trie) liste_choisi False) liste_choisi False)
  | [] -> creerArbre Nul (select animaux_rest sens) Nul
;;

let rebuild_pancanator liste couleur file=
  (build_pancanator liste [] Blank), liste,couleur,file;;
