exception AjoutData__Animal_already_exist of string
exception Plouc 

(*
BUT:demander le nom d'un nouvel animal à l'utilisateur
PRECONDITION: listAnimaux   ->lsite de chaine de caractère (liste de tous les animaux existant deja)
			  couleur 		-> Couleur

POSTCONDITION:renvoie une chaine de caratère (animal) qui n'existe pas encore rentré par l'utilsateur
*)

let rec newAnimal = fun listAnimaux couleur->
	
	let animal = lecture "A quel animal pensiez vous?" couleur in (*on demande à l'utilsateur*)
	if( List.exists (fun a -> (a = animal)) listAnimaux) (*si il existe deja*)
	then
	begin
		log ("[AJOUT]: <"^animal^"> existe deja");
		newAnimal listAnimaux couleur (*on redemande*)
	end
	else
		animal (*sinon c'est ok*)
;;

(*
BUT:creer un historique complet pour chaque attribut
PRECONDITION: historique 	-> liste de couple (chaine de caractère,ternaire)
			  dataList 		-> liste de Data
			  attributList 	-> liste de couple (chaine de caractère,ternaire) (initialisation à vide)

POSTCONDITION:renvoie une liste de couple (chaine de caractère,ternaire) où chaque attribut possible apparait avec une valeur
*)


let rec convertHisto = fun historique dataList attributList->
	match dataList with
	| [] -> attributList (*si on a parcouru toute la liste de data c'est terminé*)
	| Data(att,_)::r -> (*sinon*)
		try convertHisto historique r (attributList@[(att,(snd (List.find (fun (a,_) -> (att = a)) historique)))]) with (*si l'attribut en cours existe deja dans l'historique on prend la valeur de l'historique*)
		| Not_found -> convertHisto historique r (attributList@[(att,Blank)]) (*sinon on met blank*)
	
;;

(*
BUT:Ajouter un nouvelle animal
PRECONDITION: animal 		-> chaine de caractère (animal à mettre à jour) , ce doit être un nouvelle animal
			  attributList 	-> liste de couple (chaine de caractère,ternaire) 
			  dataList 		-> liste de Data
			  dataListFin 	-> liste de Data (initialisation à vide)
POSTCONDITION:met a jour et sauvegarde la liste de Data en mettant les attribut à vrai pour l'animal à faux pour l'ancien et à l'ancienne valeur ou blank pour les autres animaux
*)



let rec addAnimal = fun animal attributList dataList dataListFin->
	match dataList with
	| [] -> dataListFin(*si on a parcouru toute la liste de data c'est terminé*)
	| Data(att,l)::r -> 
		try addAnimal animal attributList r (dataListFin@[Data(att,l@[(animal,(snd (List.find (fun (a,_) -> att = a) attributList)))])]) with(*si l'attribut en cours existe deja dans attributList on prend la valeur presente dans attributList*)
		| Not_found -> addAnimal animal attributList r (dataListFin@[Data(att,l@[(animal,Blank)])])(*sinon on met blank*)
;;

(*
BUT:Mettre a jour des attributs sur un animal
PRECONDITION: dataList 		-> liste de Data
			  dataListFin 	-> liste de Data (initialisation à vide)
			  attribut 		-> liste de couple (chaine de caractère,ternaire), seul les attributs existant deja seront mis à jour 
			  animal 		-> chaine de caractère (animal à mettre à jour) , ce doit être un animal existant
			  animalFin 	-> chaine de caractère (ancien animal)
			  animaux 		-> liste des animaux (chaine de caractère)
POSTCONDITION:met a jour la liste de Data en mettant les attribut à vrai pour l'animal à faux pour l'ancien et à l'ancienne valeur ou blank pour les autres animaux
*)


let rec attributUpdatev2 = fun dataList dataListFin attribut animal animaux->
	match dataList with
	| [] -> dataListFin(*si on a parcouru toute la liste de data c'est terminé*)
	| (Data(att,listAnimaux)::r) ->
		(*on met a jour l'attribut en cours... on parcours tous les animaux...*)
		attributUpdatev2 r (dataListFin@[Data(att,(List.fold_left (fun a (b,tern)-> if (b = animal) then a@[(b, (*si on est sur l'animal a updater*)
		(
		try snd (List.find (fun (a,_) -> a=att) attribut) with (*si l'attribut est defini, on le met a jour*)
		| Not_found -> tern (*sinon on garde l'ancien attribut*)
		))] else a@[(b,tern)]) [] listAnimaux))]) attribut animal animaux (*sinon on change rien*)
;;

(*
BUT:Mettre a jour des attributs sur un animal
PRECONDITION: dataList 		-> liste de Data
			  attribut 		-> liste de couple (chaine de caractère,ternaire), seul les attributs existant deja seront mis à jour 
			  animal 		-> chaine de caractère (animal à mettre à jour) , ce doit être un animal existant
			  animalFin 	-> chaine de caractère (ancien animal)
			  couleur 		-> couleur
POSTCONDITION:met a jour et sauvegarde la liste de Data en mettant les attribut à vrai pour l'animal à faux pour l'ancien et à l'ancienne valeur ou blank pour les autres animaux
*)




let rec attributUpdate = fun dataList attribut animal animalfin couleur file->
	log ("[UPDATE]:mise a jour des attribut pour "^animal);
	let newdata = attributUpdatev2 dataList [] attribut animal (animaux (List.hd dataList)) in (*on recupère la mise à jour*)
	log "[UPDATE]:enregistrement des nouveaux attributs...";
	save newdata file;(*on l'enregistre*)
	newdata (*et on renvoie la nouvelle liste de data*)
;;



(*
BUT:Ajoute un nouvel attribut à une Liste de Data
PRECONDITION: dataList 		-> liste de Data
			  dataListFin 	-> liste de Data (initialisation à vide)
			  attribut 		-> chaine de caractère 
			  animal 		-> chaine de caractère (nouvel animal)
			  animalFin 	-> chaine de caractère (ancien animal)
			  animaux 		-> liste des animaux (chaine de caractère)
POSTCONDITION:met a jour la liste de Data en mettant l'attribut à vrai pour le nouvel animal à faux pour l'ancien et à l'ancienne valeur ou blank pour les autres animaux
*)

let rec newAttribut = fun dataList dataListFin attribut animal animalFin animaux->
	match dataList with
	(*si on arrive a la fin et que l'attribut a été ajouté c'est bon*)
	| [] when (attribut = "-") -> dataListFin
	(*si on arrive a la fin et que l'attribut a pas été ajouté on l'ajoute... on le met a true pour le nouvel animal, false pour l'ancien et blank pour les autres*)
	| [] -> (dataListFin@[Data(attribut,(List.fold_left (fun a b->  if (b = animal) then a@[(b,True)] else if (List.exists (fun an -> an = b ) animalFin) then a@[(b,False)] else a@[(b,Blank)]) [] animaux)@[])])
	(*si on est sur l'attribut, on l'update à vrai pour le nouvelle animal, faux pour l'ancien et on change rien pour les autres*)
	| Data(att,listAnimaux)::r when (att = attribut) -> newAttribut r (dataListFin@[Data(attribut,(List.fold_left (fun a (b,tern)-> if (b = animal) then a@[(b,True)] else if (List.exists (fun an -> an = b ) animalFin) then a@[(b,False)] else a@[(b,tern)]) [] listAnimaux))]) "-" animal animalFin animaux
	(*si on est pas sur l'attribut, on continue*)
	| Data(att,listAnimaux)::r -> newAttribut r (dataListFin@[Data(att,(listAnimaux))]) attribut animal animalFin animaux
;;

(*
BUT:Ajouter un nouvel animal à la BDD
PRECONDITION: dataList 		-> liste de Data
			  historique 	-> liste de couple (string,ternaire)
			  animalfin 	-> string
			  couleur 		-> Couleur
POSTCONDITION:met a jour la BDD; renvoie l'arbre actualisé avec le nouvel animal
*)


let ajoutAnimal = fun dataList historique animalFin couleur file->
	
	log "[AJOUT]:génération de la liste des animaux";
	let listAnimaux = animaux (List.hd dataList) in (*on recupère la liste d'animaux*)
	log "[AJOUT]:demande de l'animal à l'utilisateur";
	let animal = newAnimal listAnimaux couleur in (*demande à l'utilisateur quel animal il veut ajouter*)
	log "[AJOUT]:demande de l'attribut à ajouter";
	let attribut = lecture ("Quel attribut a t'il ?") couleur in(*demande à l'utilisateur le nouvel attribut*)
	log "[AJOUT]:insertion du nouvel attribut";
	let dataListup = newAttribut dataList [] attribut animal animalFin listAnimaux in (*ajoute à la liste de data le nouvel attribut*)
	log "[AJOUT]:creation de la liste d'attribut";
	let attributList = (convertHisto (historique@[(attribut,True)]) dataListup []) in(*met en place un historique des données plus complet*)
	log "[AJOUT]:insertion du nouvel animal";
	let newDataList = addAnimal animal (choose attributList) dataListup [] in(*met a jour la liste de data avec le nouvel animal en demandant les attribut à l'utilisateur*)
	log "[AJOUT]:enregistrement des nouvelles données";
	save newDataList file;(*enregistre dans le fichier*)
	log "[AJOUT]:redemarrage de la partie";
	rebuild_pancanator newDataList couleur file(*relance le jeu*)
;;  



let fusionListe = fun l1 l2 -> List.fold_left (fun a b -> if (List.exists (fun c -> c=b) a) then (a) else (a@[b])) l2 l1;;

(*

let rec listeattribut f1 = List.fold_left (fun l Data(a,b) -> l@[a]) [] f1;;   


let rec casecase = fun f1 animal attribut ->
match f1 with
| patt -> expr
| _ -> expr2
| [] -> Blank 
| Data(a,l)::r when (a = attribut) then try snd (list.find (fun (aa,ba) -> aa = animal)) l with
										| Not_found -> Blank
| a::r -> casecase r animal attribut;; 



let rec fufu = fun f1 f2 animaux1 animaux2 animal attribut->
let c1 = casecase f1 animal attribut;;
let c2 = casecase f2 animal attribut;;
if (c1 = Blank)
then
c2
else if(c2 = Blank)
then
c1
else if (c1 != c2)
then
Blank
else
c1
;;





let rec fusionDoubleRec = fun f1 f2 animaux1 animaux2 animauxFusion attributfusion final->
match attributfusion with
| [] -> final
| a::r -> fusionDoubleRec f1 f2 animaux1 animaux2 animauxFusion r (final@[attribut,List.fold_left (fun a b -> a@[b,(fufu f1 f2 animaux1 animaux2 b attribut)]
) [] animauxFusion])

;;

let fusionDouble = fun f1 f2 ->
	fusionDoubleRec f1 f2 animaux(f1) animaux(f2) (fusionListe (fusionListe [] animaux(f1)) animaux(f2)) (fusionListe (listeattribut f1) (listeattribut f2)) []
;;


let rec fusion = fun fileList out->
	match fileList with
	a::r -> fusionDouble (ouvre ("toto",!color,a) (fun o p q->o) ) (ouvre ("toto",!color,fusion r out) (fun o p q-> o) ) 
	[] -> out
;;
*)