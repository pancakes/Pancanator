(**)

let changeProfil utilisateur = 
	let newName = 
	(print_string "Nouveau nom?\n");
	read_line();
in
	match utilisateur with
|(nom,coul,data) -> (newName,coul,data)
;;

(**)

let changementcouleur newCouleur utilisateur = 
	match utilisateur with
(nom,coul,data) -> (nom,newCouleur,data)
;;


let rec changecouleur utilisateur =
	let newCouleur = (print_string ("nouvelle couleur de l'interface entre : rouge,bleu,orange,vert,jaune,rose,marron,violet,turquoise\n"));
					 read_line();
in
	match newCouleur with
"rouge" -> (changementcouleur ROUGE utilisateur)
|"bleu" -> (changementcouleur BLEU utilisateur)
|"orange" -> (changementcouleur ORANGE utilisateur)
|"vert" -> (changementcouleur VERT utilisateur)
|"jaune" -> (changementcouleur JAUNE utilisateur)
|"rose" -> (changementcouleur ROSE utilisateur)
|"marron" -> (changementcouleur MARRON utilisateur)
|"violet" -> (changementcouleur VIOLET utilisateur)
|"turquoise" -> (changementcouleur TURQUOISE utilisateur)
| _ -> changecouleur utilisateur
;;


let creationFile file = 
	close_out (open_out file);
	true
;;


let rec creerfichier = fun file ->
	print_string ("Le fichier <"^file^"> n'existe pas, le creer? (o/n) \n");
	let reponse = read_line() in
	if (reponse = "o") || (reponse = "oui") then creationFile file
	else if (reponse = "non") || (reponse = "n") then false
	else creerfichier file 

;;

let rec changeFichier utilisateur =
	let newData = (print_string "Nom du fichier?\n");
					 read_line();
	in
	if (Sys.file_exists newData) 
	then
		match utilisateur with
			|(nom,coul,data) -> (nom,coul,newData)
	else
		if (creerfichier newData)
		then

			match utilisateur with
				|(nom,coul,data) -> (nom,coul,newData)
		else
		 	changeFichier utilisateur		

;;



let rec changeOptions utilisateur =
	let reponce =
	(print_string ("\n<< Options >>"^"\n"^"profil (1)"^"\n"^"couleur (2)"^"\n"^"Fichier utilisé (3)"^"\n"^"Sortir (4)"^"\n"));
	read_line();
in
match reponce  with
"1" -> (changeOptions (changeProfil utilisateur))
|"2" -> (changeOptions (changecouleur utilisateur))
|"3" -> (changeOptions (changeFichier utilisateur))
|"4" -> (utilisateur)
|_ -> (changeOptions utilisateur)
;;

let rec mainpage utilisateur= 
	(print_string ("\n### ### Pancanator ### ###\n\n"^"Jouer(1)/option(2)/Exit(3)"^"\n") );
match read_line() with
"1" -> retry (ouvre utilisateur rebuild_pancanator);print_string("") (*retry (ouvre data rebuild_pancanator)*)
|"2"-> (mainpage (changeOptions utilisateur))
|"3" -> (print_string "Fin de la game\n")
|_ -> (mainpage utilisateur)
;;

