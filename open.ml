exception Analyse_Corrompus

let classe = Data("nageoire",["tom",False;"elio",Blank]);;
let dataNil = Data("",[]);;

(*
BUT:convertir une ligne de fichier de donnée en data
PRECONDITION: ligne -> chaine de caractère au format attribut;ternaire;ternaire;...
              animaux -> liste de chaines de caractères (listes des animaux possible)
              attribut -> chaine de caractère
              l -> liste de (animaux,ternaire)
POSTCONDITION: renvoie une data
*)


let rec analyse = fun ligne animaux attribut l->
  match ligne,animaux with
  | [],[]-> Data(attribut,l) 
  | [],_->raise Analyse_Corrompus
  | _,[] ->raise Analyse_Corrompus
  | a1::r1,a2::r2 ->
    if(a2 = "attribut") 
    then analyse r1 r2 a1 []
    else analyse r1 r2 attribut (l@[a2,(ternaire_of_str a1)]) 
;;


(*
BUT:charger un fichier de données pour le convertir en dataList
PRECONDITION: flux -> flux in
              nlignes -> entier, initialisé à 0 (represente le numero de ligne en train d'être lu)
              l -> liste de data finale
              animaux -> liste de chaine de caractères
POSTCONDITION: renvoie une liste de data
*)


let rec lectureFile = fun flux nligne l animaux->
  try
    if(nligne = 0)(*si in lit la 1ere ligne*)
    then
      lectureFile flux (nligne +1) l (Str.split (Str.regexp ";") (input_line flux))(*on la recupère, c'est la liste des animaux*)
    else
      lectureFile (flux) (nligne + 1) ((analyse (Str.split (Str.regexp ";") (input_line flux)) animaux "" [])::l) animaux(*sinon on la decompose et on la convertit en data*)
  with End_of_file -> l
;;

(*
BUT:initialiser un fichier de données vide
PRECONDITION: name -> nom du fichier
POSTCONDITION: renvoie un unit; remplit le fichier avec un attribut et deux animaux demandés à l'utilisateur
*)


let initBase = fun name couleur->

  log ("[FICHIER]:initialisation du fichier <"^name^">...\n");
  let attribut = lecture "Rentrez un attribut?" couleur in
  let animalNon = lecture "Rentrez un animal sans cet attribut?" couleur in
  let animalOui = lecture "Rentrez un animal avec cet attribut?" couleur in
  log ("[FICHIER]:enregistrement du fichier <"^name^">...");
  save [Data(attribut,[(animalNon,False);(animalOui,True)])] name
;;

(*
BUT:charger un fichier de données pour le convertir en dataList
PRECONDITION: name -> nom du fichier
                 f -> fonction qui prend en paramètre la dataList
POSTCONDITION:renvoie la sortie de la fonction f
*)

let rec ouvre = fun (profil,couleur,name) f->
  log ("[INTERFACE]:ouverture de la fenètre graphique en 700*350...\n");
  Graphics.open_graph " 700x350"; (*on demarre l'interface*)  
  log ("[FICHIER]:ouverture du fichier <"^name^">...\n");
  let flux = open_in name in (*on ouvre le fichier*)
  log ("[FICHIER]:lecture du fichier <"^name^">...\n");
  let data = (lectureFile flux 0 [] []) in (*on le convertit en data liste*)
  log("[FICHIER]:fermeture du fichier <"^name^">...\n");
  close_in flux; (*on ferme le fichier*)
  if( data = [] )(*si le fichier est vide*)
  then
  begin
    log ("[FICHIER]:le fichier <"^name^"> est vide\n");
    initBase name couleur;(*on l'inialise*)
    ouvre (profil,couleur,name) f (*on réessaye de le lire*)
  end
  else
    f data couleur name (*sinon on execute la foction avec la liste de data généré*)
  
;;
