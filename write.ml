let log = fun msg ->
  if !verbose then
    print_string(msg);
    flush stdout
;;

let animaux = fun (Data(att,l)) -> 
	 List.fold_left (fun a b -> a@[fst b]) [] l
;;


let rec ecriture = fun flux dataList ->
	match dataList with
	| [] -> ()
	| Data(att,l)::r -> output_string flux ((List.fold_left (fun a (x,y) -> a^";"^(string_of_ternaire y)) att l)^"\n"); ecriture flux r
;;

let save = fun dataList name->
	let flux = open_out name in
	output_string flux ((List.fold_left (fun a b -> a^";"^b) "attribut" (animaux (List.hd dataList)))^"\n");
	ecriture flux dataList;
  	close_out flux
;;
