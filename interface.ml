#load "graphics.cma";;


let background = fun color1 color2 ->
	let x = Graphics.size_x() in 
	let y = Graphics.size_y() in
	Graphics.set_color color1;
	Graphics.fill_rect 0 0 x y;
	Graphics.set_color color2
;;

let clear = fun () ->
	Graphics.clear_graph();(*on efface tout*)
	Graphics.set_window_title "Pancanator";(*titre de la fenetre*)

	background (Graphics.white) (Graphics.black)(*le foncd de la fenetre en blanc*)
;;


(*
Précondition:
->c:caractère
->l:entier positif
Postcondition:
->créé une chaine de caractère de longueur l composé de caractères c
*)

let rec longstr = fun c l ->
	match l with
	| 0 -> Char.escaped c
	| _ -> (Char.escaped c)^longstr c (l-1)
;;


let coche = fun x0 y0 color->
	Graphics.set_color(color);
	Graphics.fill_rect (x0) (y0) (10) (10);
;;

let decoche = fun x0 y0 ->
	Graphics.set_color(Graphics.white);
	Graphics.fill_rect (x0) (y0) (10) (10);
	Graphics.set_color(Graphics.black);
	Graphics.draw_rect (x0) (y0) (10) (10)
;;



let fleche = fun x y -> 
	Graphics.moveto (x) y;
	Graphics.draw_string ">";
	Graphics.moveto (x+2) y;
	Graphics.draw_string ">";
	Graphics.moveto (x+4) y;
	Graphics.draw_string ">"
;;



let setCase = fun x y (attribut,valeur) select i->
	Graphics.set_color(Graphics.black);
	Graphics.moveto (x+20) y;
	Graphics.draw_string attribut;
	if(select = i) then 
	begin			
			Graphics.set_color(Graphics.black);
			fleche (x-12) y
	end;		

	if( valeur = True) then
		coche x y (Graphics.green)
	else if(valeur = False) then
		coche x y (Graphics.red)
	else
		decoche x y
;;






let rec affCase = fun cases x y x0 select i-> 
	let max = Graphics.size_x() in 
	match cases with
	| [] -> ()
	| (attribut,valeur)::[] -> 
		setCase x y (attribut,valeur) select i;
	| (attribut,valeur)::(attributSuiv,valeurSuiv)::caseSuiv -> 
		setCase x y (attribut,valeur) select i;
		 

		if(((x+80+(String.length attribut)*6)+(String.length attributSuiv)*6) > max)
		then	
			affCase ((attributSuiv,valeurSuiv)::caseSuiv) (x0)(y-15) x0 select (i+1)
		else
			affCase ((attributSuiv,valeurSuiv)::caseSuiv) (x+ 40+((String.length attribut)*6)) (y) x0 select (i+1)	
;;

let rec changeCase = fun l select i newValeur->
	match l,i with
	| [],_ -> []
	| ((attribut,valeur)::r),n when (n = select) -> (attribut,newValeur)::r
	| a::r,_ -> a::(changeCase r select (i+1) newValeur)
;;

let title = fun titre x y x2->
	Graphics.set_color(Graphics.black);
	(*dessin du cadre autour du titre*)
	Graphics.draw_rect (x-5) (y-30) (x2+35) (20);
	Graphics.draw_rect (x-6) (y-31) (x2+37) (22);
	
	(*ecriture du titre*)
	Graphics.moveto x (y-25);
	Graphics.draw_string ("=== "^titre^" ===")
;;


let rec choose02 = fun l xprec yprec modif select max->
	let x = Graphics.size_x() in 
	let y = Graphics.size_y() in
	
	
	(*affichage *)
	if((x != xprec) || (y != yprec) || (modif))
	then
	begin
		clear();
		affCase l 15 (y-70) 15 select 1;
		title "Ses attributs" ((x-(19*7))/2) (y) (int_of_float((float_of_int(13)+.3.)*.6.5));
		Graphics.moveto ((x-(66*7))/2) (y-45);
		Graphics.draw_string "o:Oui n:Non b:Blank - s/d:suivant - q/z:precedent - entree:valider"

	end;
		

	(*interaction utilisateur*)
	if Graphics.key_pressed () then

  		match Graphics.read_key () with
  		| t when (t = Char.chr 13) -> l 
  		| 'y' | 'o' | 'Y' | 'O' ->choose02 (changeCase l select 1 True) x y true (select) max
  		| 'n' | 'N' -> choose02 (changeCase l select 1 False) x y true (select) max
  		| 'b' | 'B' -> choose02 (changeCase l select 1 Blank) x y true (select) max
  		| 'z' | 'Z' | 'q' | 'Q' when (select > 1)-> choose02 l x y true (select-1) max(*haut/gauche*)
  		| 's' | 'S' | 'd' | 'D' when (select < max)-> choose02 l x y true (select+1) max(*bas/droite*)
  		| t -> choose02 l x y false (select) max(*autre touche*)
	else
		choose02 l x y false (select) max (*aucune touche, on attends*)			
;;

let choose = fun l -> choose02 l 0 0 true 1 (List.length l);;





let rec degrade = fun coul x y x2 y2 c2 n->
	Graphics.set_color (Graphics.rgb coul coul coul);
	Graphics.fill_rect x y x2 y2 ;
	if(n > 0) then degrade (coul-c2) (x+1) (y+1) (x2-2) (y2-2) c2 (n-1)
;;


(*
Précondition: 
->x:entier non nul
->y:entier non nul
->str:chaine de caractère
->l:longueur de str
->x2:coordonnées de la fenètre centrée
Postcondition: affiche l'interface au coordonnées x et y
*)



let settitle = fun x y str l x2->
	let gris = Graphics.rgb 210 210 210 in
	
	(*on efface l'ecran*)
	clear ();


	degrade 255 (x-23) (y-115) (x2+78) (108) 30 7;

	

	(*le cadre au centre en gris*)
	Graphics.set_color gris;
	Graphics.fill_rect (x-25) (y-105) (x2+70) (100);
	Graphics.set_color (Graphics.black);

	(*le titre*)
	title str x y x2
;;




let setCouleur = fun couleur ->
match couleur with
| ROUGE -> Graphics.rgb 195 30 30
| BLEU -> Graphics.rgb 47 40 198
| VERT -> Graphics.rgb 8 133 38
| JAUNE -> Graphics.rgb 193 166 0
| ROSE -> Graphics.rgb 220 42 164
| MARRON ->Graphics.rgb 99 58 0
| VIOLET -> Graphics.rgb 63 12 148
| TURQUOISE -> Graphics.rgb 2 176 166
| ORANGE -> Graphics.rgb 223 100 10

;;



(*
Précondition: 
->x:entier non nul
->y:entier non nul
->str:chaine de caractère
->result:booleen
->l:longueur de str
Postcondition: affiche l'interface (oui/non) au coordonnées x et y
*)

let dessin = fun x y str result l couleur->
	let x2 = int_of_float((float_of_int(l)+.3.)*.6.5) in(*position centré du texte*)
	settitle x y str l x2;(*affichage du titre*)
	let couleurCase = setCouleur couleur in
	(*affichage du menu de reponse*)
	

	if result (*placement du curseur au bon endroit*)
	then 
	begin

		Graphics.set_color (couleurCase);
		fleche x (y-60);

		Graphics.fill_rect (x+25) (y-62) 35 16;
		Graphics.set_color (Graphics.white);
		Graphics.moveto x (y-60);
		Graphics.draw_string ("     OUI");
		Graphics.set_color (Graphics.black);
		Graphics.draw_rect (x+24) (y-63) 37 18;
		Graphics.moveto x (y-80);
		Graphics.draw_string ("     NON");
	end
	else
	begin
		
		
		Graphics.set_color (couleurCase);
		fleche x (y-80);
		Graphics.fill_rect (x+25) (y-82) 35 16;
		Graphics.set_color (Graphics.black);
		Graphics.draw_rect (x+24) (y-83) 37 18;		
		Graphics.moveto x (y-60);
		Graphics.draw_string ("     OUI");
		Graphics.set_color (Graphics.white);
		Graphics.moveto x (y-80);
		Graphics.draw_string ("     NON");
	end
	
	
	

;;








(*
Précondition: 
->x:entier non nul
->y:entier non nul
->str:chaine de caractère
->result:booleen
->l:longueur de str
Postcondition: affiche l'interface (input) au coordonnées x et y
*)

let dessinQuest = fun x y str result l couleur ->
		
	let x2 = int_of_float((float_of_int(l)+.3.)*.6.5) in(*position centré du texte*)
	let couleurCase = setCouleur couleur in

	settitle x y str l x2;(*affichage du titre*)

	(*affichage du cadre pour la reponse*)
	Graphics.set_color (Graphics.white);
	Graphics.fill_rect (x-6) (y-65) (x2+35) (20);

	Graphics.set_color (couleurCase);
	Graphics.draw_rect (x-7) (y-66) (x2+37) (22);

	(*affichage de la reponse*)
	Graphics.set_color(Graphics.black);
	Graphics.moveto x (y-60);
	Graphics.draw_string ("> "^result);
;;

(*
Précondition: 
->str:une chaine de caractère
az->result:booleen
->resultprec:booleen
->xprec:entier non nul
->yprec:entier non nul
Postcondition: pose la question dans une fenètre graphique et renvoie un booleen selon la reponse utilisateur
*)
let rec lecture02 = fun question result xprec yprec modif couleur->

(*Récupération de la taille de la fenètre*)
let x = Graphics.size_x() in 
let y = Graphics.size_y() in

(*longueur de la question*)
let l = String.length question in
(*longueur de la reponse*)
let l2 = String.length result in

(*coordonnées de la question pour la centrer*)
let xrel = ((x-(l*7))/2) in

(*si l'etat de la fenètre a changé, on actualise l'affichage*)	
if ( (xprec != x) || (yprec != y) || modif )
then
	(*si la reponse ne depasse pas le cadre*)
	if(String.length result < (l + 8)) 
	then dessinQuest (xrel-20) (y-20) question result l couleur(*on l'affiche*) 
	else dessinQuest (xrel-20) (y-20) question (String.sub result (l2-8-l) (l+8)) l couleur;(*on n'affiche que la derniere partie de la reponse*) 

(*si une touche a été préssée*)
if Graphics.key_pressed () then

  match Graphics.read_key () with
  | t when (t = Char.chr 13) -> if(result != "") then result else lecture02 question result x y false couleur  (*touche entrée*)
  | t when ((t = Char.chr 8) && (l2 > 0)) -> lecture02 question (String.sub result 0 ((String.length result)-1)) x y true couleur(*touche retour si la chaine n'est pas vide*)
  | t when (t = Char.chr 8)  -> lecture02 question result x y false couleur (*touche retour si la chaine est vide*)
  | t when (l2 < (l + 8)) -> Graphics.draw_char t;lecture02 question (result^(Char.escaped t)) x y false couleur (*une autre touche si la question ne depasse pas du cadre*)
  | t -> lecture02 question (result^(Char.escaped t)) x y true couleur(*une autre touche si la reponse depasse du cadre*)
else
	lecture02 question result x y false couleur (*aucune touche, on attends*)
;;




let rec window02 = fun str result resultprec xprec yprec couleur->

(*Récupération de la taille de la fenètre*)
let x = Graphics.size_x() in 
let y = Graphics.size_y() in

(*longueur de la question*)
let l = String.length str in

(*coordonnées de la question pour la centrer*)
let xrel = ((x-(l*7))/2) in

(*si l'etat de la fenètre a changé, on actualise l'affichage*)	
if ((result != resultprec) || (xprec != x) || (yprec != y))
then
	dessin (xrel-20) (y-20) str result l couleur;


(*si une touche a été préssée*)
if Graphics.key_pressed () then

  match Graphics.read_key () with
  | t when (t = Char.chr 13) -> result (*touche entrée*)
  | 'z' | 'Z' | 's' | 'S' -> window02 str (not result) result x y couleur(*haut ou bas*)
  | t -> window02 str result result x y couleur (*une autre touche*)
else
	window02 str result result x y couleur (*aucune touche, on attends*)
;;

(*
Précondition: str est une chaine de caractère
Postcondition: pose la question dans une fenètre graphique et renvoie un booleen selon la reponse utilisateur
*)

let rec window = fun str couleur -> window02 str true false 1 1 couleur;;

let rec lecture = fun str couleur -> 
let a = lecture02 str "" 1 1 true couleur in
if(String.length a > 0)
then
a
else
lecture str couleur
;;