exception Not_a_ternaire of string;;

type ternaire = True | False | Blank;;

type data = Data of string * ((string * ternaire) list);;

type 'a arbre = Nul | Noeud of 'a arbre * 'a * 'a arbre;;

type couleur = ROUGE | BLEU | ORANGE | VERT | JAUNE | ROSE | MARRON | VIOLET | TURQUOISE;;

type joueur = string*couleur*string;;

let creerArbre gauche root droit = Noeud(gauche,root,droit);;

(*
BUT:convertir un ternaire en chaine de caractère
PRECONDITION: -> valeur : ternaire
POSTCONDITION:renvoie une chaine de caractère valant TRUE,FALSE ou BLANK
*)


let string_of_ternaire = fun valeur ->
	match valeur with
	| True -> "TRUE"
	| False -> "FALSE"
	| Blank -> "BLANK"
;;

(*
BUT:convertir une chaine de caractère en ternaire
PRECONDITION: -> valeur : string pouvant representer un ternaire
POSTCONDITION:renvoie un ternaire
*)

let ternaire_of_str = fun valeur -> 
	match valeur with
	| "true" | "vrai" | "TRUE" | "VRAI" -> True
	| "false" | "faux" | "FALSE" | "FAUX" -> False
  	| "blank" | "blanc" | "inconnu" | "unknown" | "BLANK" | "BLANC" -> Blank 
  	| _ -> raise (Not_a_ternaire valeur)
;;


(*
BUT:convertir un booleen en ternaire
PRECONDITION: -> valeur : un booleen
POSTCONDITION:renvoie un ternaire valant soit True soit False
*)

let ternaire_of_bool = fun valeur -> if(valeur) then True else False ;;
