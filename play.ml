(*Auteur: Buisso Xavier
  Date: 3/12/2015
  Fonction: Permet de définir des exception  
*)

exception Arbre_Nul;;
exception Trop_De_Valeur;;
exception Liste_Vide;;
exception Arbre_Mal_Construit;;


(*Auteur : Buisso Xavier
  Date :15/12/2015
  Fonction : demande à un utilisateur  si dans une liste de chaine de caractère l'animal auquel il pense y est.
  Précondition : dataliste est une liste de data 
  				 noeud est une liste de chaine de caractère qui est la liste d'animaux qui correspond à une même question
  				 save-liste est une liste de couple de trenair et d'attribut qui est une liste de cahine de caractère 
  				 saveListeAnimaux est la liste de chaine de caractère qui correspond à la liste de tout les animaux auquel l'utilisateur ne pense pas.
  Postcondition : Dois retourner un arbre de liste de chaine de caractère avec l'animal rajouté 
*)


let  rec compareAnimal dataliste noeud save_liste saveListeAnimaux couleur file=
	match noeud with
[] -> raise Liste_Vide
|a::[] -> 	
	if (window ("L'animal auquel tu pense est "^a) couleur) then (*on détermine si l'animal que l'utilisateur à choisie est le bon*)
		((build_pancanator (attributUpdate dataliste save_liste a [] couleur file) [] Blank), dataliste,couleur,file)
	else	                
		(ajoutAnimal dataliste save_liste (saveListeAnimaux@[a]) couleur file)
|a::r -> 	if (window ("L'animal auquel tu pense est "^a) couleur) then (*on détermine si l'animal que l'utilisateur à choisie est le bon*)
		((build_pancanator (attributUpdate dataliste save_liste a [] couleur file) [] Blank), dataliste,couleur,file)
	else	
		(compareAnimal dataliste r save_liste (saveListeAnimaux@[a]) couleur file)
;;


(*Auteur : Buisson Xavier
  Date : 3/12/205
  Fonction : Détermine quelle animal à été choisie est le bon en fonction de la question choisie
  Précondition : ar est un arbre qui comportes de liste de chaine de caractère 
  Postcondition : retourne vrai si l'utilisateur veux recommancer le jeux, faux si il préfère arrêter 
*)


let rec recherche ar save_liste dataliste couleur file=
	match ar with
Noeud(Nul,root,Nul) -> (compareAnimal dataliste root save_liste [] couleur file)
|Noeud(Nul,root,fd) -> raise Arbre_Mal_Construit
|Noeud(fg,root,Nul) -> raise Arbre_Mal_Construit
|Noeud(fg,root,fd) -> 
	if (window (List.hd root) couleur) then
		((recherche fg (save_liste@[((List.hd root),True)]) dataliste) couleur file)
	else
		((recherche fd (save_liste@[((List.hd root),False)]) dataliste) couleur file)
|Nul -> raise Arbre_Nul
;;

(*Auteur : Buisson Xavier
  Date : 3/12/205
  Fonction : Vérifie si le joueur veux rejouer ou pas
  Précondition : ar est un arbre qui comportes de liste de chaine de caractère  
  Postcondition : retourne vrai si l'utilisateur veux recommancer le jeux, faux si il préfère arrêter 
*)


let rec retry (ar,dataliste,couleur,file)  = 
  if !afarbre then affiche ar (fun  x -> List.fold_left (fun b a -> b ^ " | " ^ a) "" x);
	if (window "Tu veux jouer ? Aller viens jouer ! Ne me laisse pas tout seul !!!!" couleur) then 
		(retry (recherche ar [] dataliste couleur file)) 
	else Graphics.close_graph();;
