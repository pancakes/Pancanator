# Un programme qui __devine__ ce à quoi vous pensez

_Pancanator_ est programme dont l'objectif est de deviner l'animal auquel pense 
l'utilisateur à partir d'une base de données comprenant des animaux et leurs 
attributs particulier correspondant. Le programme étend sa base avec de 
nouvelles informations lorsque l'utilisateur propose un animal qui n'est pas 
encore dedans.

_Pancanator_ comprend une __interface graphique configurable__, un jeu de 
données avec de __nombreux animaux__, est capable de prendre de __nouvelles 
bases__ et de __les fusionner__ et propose un animal à l'utilisateur __en posant
le moins de questions possibles__.

## Lancement du jeu
### Pré-requis
#### Système d'exploitation
_Pancanator_ fonctionne sur tous système d'exploitation qui possède les 
dépendances du programme. Cela inclus Linux, OSX et Windows.
#### Dépendances
_Pancanator_ est écrit en __ocaml__[^ocaml], vous devez donc avoir installé 
l'interpréteur ocaml dans la version minimale 3.12.0 sur votre machine.

Afin de lancer le jeu, vous devez avoir tout les fichier disponibles  sur le 
repertoire git du projet : http://gitlab.etude.eisti.fr/pancakes/Pancanator

### Démarrage
Pour démarrer le jeu, ouvrez un terminal, placez vous dans le repertoire dans 
lequel vous avez téléchargé les fichier, puis faites simplement :
``` bash
ocaml main.ml
```
## Jouer
Au lancement, vous pouvez commencer directement le jeu en tapant `1` puis entrée. Faites `2` pour configurer le jeu et `3` pour annuler.
### Jeu
Choisissez un animal, puis pour chaque attribut que le jeu vous propose, répondez si oui ou non l'animal auquel vous pensez le possède à laide des touches `z` et `s` .

À la fin du jeu, le programme vous propose un animal. Si c'était bien celui auquel vous pensiez, dites 'oui', vous pourrez alors recommencer ou quitter le jeu. Sinon faites 'non' et le programme vous demandera d'ajouter l'animal dans la base de donnée afin qu'il soit capable de le reconnaitre la prochaine fois.  
Entrez le nom de l'animal auquel vous pensiez, appuyez sur entrée pour valider. Vous pourrez ensuite ajouter un attribut et que votre animal possède que l'animal proposé par le jeu __ne possède pas__. 

### Configuration
Certains paramètres du programme peuvent êtres modifié, et ce de deux manière différentes.
#### Dans le jeu
En choisissant l'option 2 au démarrage, vous pouvez :  

| Choix | Action                                                    |  
|-------|-----------------------------------------------------------|   
| 1     | Permet de changer le nom du joueur                        |  
| 2     | Permet de choisir la couleur du thème                     |  
| 3     | Permet de choisir le nom de la base de données à utiliser |  
| 4     | Permet de retourner au menu principal                     |  

#### Au lancement
Vous pouvez spécifier certains paramètres au programme en argument au lancement du programme. Pour avoir une liste détaillée, vous pouvez faire : 
```bash
ocaml main.ml --help
```

[^ocaml]: Objective caml, un language interprété disponnible ici : https://ocaml.org