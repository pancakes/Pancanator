#use "types.ml";;

exception Color_Does_not_exist;;

let verbose = ref false;;
let color = ref ORANGE;;
let file = ref "zoo.data";;
let file2 = ref "";;
let afarbre = ref false;;

let set_color couleur = color :=
  match couleur with
  | "rouge" -> ROUGE
  | "bleu" -> BLEU
  | "orange" -> ORANGE
  | "vert" -> VERT
  | "jaune" -> JAUNE
  | "rose" -> ROSE
  | "marron" -> MARRON
  | "violet" -> VIOLET
  | "turquoise" -> TURQUOISE
  | _ -> raise Color_Does_not_exist
;;

let param = 
  let speclist = 
      [("-v", Arg.Set verbose, "Active le mode de debuggage");
      ("-f", Arg.Set_string file, "suivi d'un fichiers. Choisi la base de données");
      ("-o", Arg.Tuple ([Arg.Set_string file ; Arg.Set_string file2]), "suivit
      de deux fichier. Fusionne deux bases de données | En beta");
      ("-arbre", Arg.Set afarbre, "Affiche une représentation de l'arbre dans la console");
      ("--color", Arg.String (set_color), "Choisi la couleur de theme")] in
  let usage_msg = "Pancanator est un pancake capable de deviner l'animal auquel
  vous pensez. Les options possibles sont :" in
  Arg.parse speclist print_endline usage_msg
;;
